#include <iostream>
#include <string.h>
#include <unistd.h>


using namespace std;

class Person {
private:
    static int cnt;
    char *name;
    int age;

public:
    static int getCount(void);

    Person(){
        this->name = NULL;
        this->cnt++;
    }
    Person(char *name){
        this->name = new char[strlen(name)+1];
        strcpy(this->name, name);
        cnt++;
    }
    Person(char *name, int age){
        this->name = new char[strlen(name)+1];
        strcpy(this->name, name);
        this->age = age; 
        cnt++;
    }
    Person(Person &per){
        this->name = new char[strlen(name)+1];
        strcpy(this->name, per.name);
        this->age = per.age; 
        cnt++;      
    }

    ~Person(){
        if(this->name){
            delete(this->name);
        }
    }

    void setName(char *name){
        if(this->name){
            delete(this->name);
        }
        this->name = new char[strlen(name)+1];
        strcpy(this->name, name);
    }

    void setAge(int age){
        this->age = age;
    }

    void printInfo(void){
        cout<<"name= "<<name<<", age= "<<age<<endl;
    }
};

// 定义并初始化
int Person::cnt = 0;

int Person::getCount(void)
{
    return cnt;
}

//student 继承person
class Student:public Person{

};

int main(int argc, char **argv)
{
    Student s;

    s.setAge(10);
    s.setName("zhangsan");
    s.printInfo();
    
    return 0;
}
