#include <iostream>
#include <string.h>


using namespace std;

class Father{
private:
    int money;

public:
    Father(){

    }

    ~Father(){

    }

    int getMoney(void){
        return this->money;
    }

    void setMoney(int money){
        this->money = money;
    }

    void itSkill(void){
        cout<<"father is it skill"<<endl;
    }
};

class Son:public Father {
private:
    int toy;

public:
    void playGame(void){
        cout<<"son play game"<<endl;
    }
};

int main(int argc, char **argv)
{
    Son s;

    s.setMoney(10);
    cout<< s.getMoney()<<endl;

    s.itSkill();

    return 0;
}