#include <iostream>
#include <string.h>
#include <unistd.h>


using namespace std;

class Point {
private:
    int mx;
    int my;

public:
    Point(){
        mx = 0;
        my = 0;
    }
    Point(int x, int y): mx(x), my(y){

    }

    int getX(void){
        return mx;
    }
    int getY(void){
        return my;
    }

    void setX(int x){
        this->mx = x;
    }
    void setY(int y){
        this->my = y;
    }
    void printIofo(void){
        cout<< "x = "<< mx << " , "<< "y = "<< my<<endl;
    }

    // 申明友元函数，可以直接访问类中的私有成员变量
    friend Point addPoint(Point &p1, Point &p2);
};

// 友元函数定义实现
Point addPoint(Point &p1, Point &p2)
{
    Point n;
    
    n.mx = p1.mx + p2.mx;
    n.my = p1.my + p2.my;

    return n;
}

Point add(Point &p1, Point &p2)
{
    Point n;
    
    n.setX(p1.getX() + p2.getX());
    n.setY(p1.getY() + p2.getY());

    return n;
}

int main(int argc, char **argv)
{
    Point p1(1, 2);
    Point p2(2, 3);

    Point sum = add(p1, p2);
    sum.printIofo();

    Point sum2 = addPoint(p1, p2);
    sum2.printIofo();

    return 0;
}