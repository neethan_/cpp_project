#include <iostream>


using namespace std;

int add(int a, int b)
{
    cout<<"add int+int"<<endl;
    return a+b;
}

double add(double a, double b)
{
    cout<<"add double+double"<<endl;
    return a+b;
}

double add(double a, double b, double c)
{
    cout<<"add double+double+double"<<endl;
    return a+b+c;
}

double add(double a, int b)
{
    cout<<"add double+int"<<endl;
    return (double)a+b;
}

double add(int b, double a)
{
    cout<<"add int+double"<<endl;
    return (double)a+b;
}

int main(int argc, char **argv)
{
    add(1,2);
    add(10.1, 2.0);

    add(1.0, 2.0, 3.0);
    add(1.0, 3);
    add(1, 4.0);

    return 0;
}