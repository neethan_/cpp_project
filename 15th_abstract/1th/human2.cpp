#include <iostream>
#include <string.h>


using namespace std;


// 一个类里有纯虚函数的话就变成了抽象类
// 这样就不能使用抽象类实例化对象
// 所有纯虚函数一定要在子类中实现，否则无法编译通过
class Human {
private:
    int a;

public:
    // 析构函数一般都申明为虚函数
    virtual ~Human(){
        cout<<"~Human()"<<endl;
    }    
    
    // 纯虚函数
    virtual void eating(void) = 0;
    virtual void wearing(void) = 0;
    virtual void driving(void) = 0;

/*
    virtual Human* test(void){
        cout<<"HUman's test"<<endl; 
        return ;
    }
*/
};

class Englishman : public Human {

public:
   virtual  ~Englishman(){
        cout<<"~Englishman()"<<endl;
    }

    // 需要实现以下虚函数
    void eating(void){
        cout<<"use knife to eat"<<endl;
    }

    void wearing(void){
        cout<<"wear english style"<<endl;
    }
    void driving(void){
        cout<<"drive english car"<<endl;
    }
};

class Chinese : public Human{

public:
    virtual ~Chinese(){
        cout<<"~Chinese()"<<endl;
    }

    void eating(void){
        cout<<"use chopsticks to eat"<<endl;
    }

    void wearing(void){
        cout<<"wear chinese style"<<endl;
    }

    // 故意注释掉，则编译将会出错，因为有虚函数没有实现。可以在其他子类实现
    // void driving(void){
    //     cout<<"drive chinese car"<<endl;
    // }
};

class Hunanman : public Chinese {
public:
    void driving(void){
        cout<<"drive Hunan car"<<endl;
    } 
};

int main(int argc, char **argv)
{
    // 抽象类不能实例化对象
    //Human h;
    Englishman e;
    Hunanman h;

    return 0;
}