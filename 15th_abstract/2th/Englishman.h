#ifndef __ENGLISHMAN_H
#define __ENGLISHMAN_H

#include <iostream>
#include <string>
#include <unistd.h>


using namespace std;

class Englishman {
private:
    char *name;

public:
     ~Englishman();

    void setName(char *name);
    char *getName(void);
    void eating(void);
    void wearing(void);
    void driving(void);
};

#endif /* __ENGLISHMAN_H */
