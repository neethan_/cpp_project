#ifndef __CHINESE_H
#define __CHINESE_H

#include <iostream>
#include <string>
#include <unistd.h>


using namespace std;


class Chinese {
private:
    char *name;

public:
    ~Chinese();
    
    void setName(char *name);
    char *getName(void);
    void eating(void);
    void wearing(void);
    void driving(void);
};

#endif /* __CHINESE_H */
