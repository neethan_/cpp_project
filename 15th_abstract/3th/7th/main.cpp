#include "Human.h"
// #include "Englishman.h"
// #include "Chinese.h"



void test_eating(Human *h)
{
    h->eating();
}


int main(int argc, char *argv[])
{
    Human &e = CreateEnglishman("Neethan", 28, "Guangdong");
    Human &c = CreateChinese("Ready", 28, "Heyuan");

    Human *h[2] = {&e, &c};
    int i;

    for(i=0; i<2; i++){
        test_eating(h[i]);
    }

    delete &e;
    delete &c;

    return 0;
}