#ifndef __HUMAN_H
#define __HUMAN_H

#include <iostream>
#include <string>
#include <unistd.h>


using namespace std;


class Human {
private:
    char *name;

public:

    void setName(char *name);
    char *getName(void);

    // 纯虚函数的好处：
    // 可以阻止生成Human实例化对象
    virtual void eating(void) = 0;
    virtual void wearing(void) = 0;
    virtual void driving(void) = 0;

    virtual ~Human(){cout<<"~Human"<<endl;}
};

Human& CreateEnglishman(char *name, int age, char *address);
Human& CreateChinese(char *name, int age, char *address);

#endif /* __HUMAN_H */
