#ifndef __ENGLISHMAN_H
#define __ENGLISHMAN_H

#include <iostream>
#include <string>
#include <unistd.h>
#include "Human.h"

using namespace std;

class Englishman : public Human {
private:
    int age;
    char address[200];

public:

    void eating(void);
    void wearing(void);
    void driving(void);
    Englishman();
    Englishman(char *name, int age, char *address);
    ~Englishman();
};

#endif /* __ENGLISHMAN_H */
