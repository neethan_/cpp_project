#ifndef __CHINESE_H
#define __CHINESE_H

#include <iostream>
#include <string>
#include <unistd.h>
#include "Human.h"


using namespace std;


class Chinese : public Human{

public:
    
    void eating(void);
    void wearing(void);
    void driving(void);
    ~Chinese();
};

#endif /* __CHINESE_H */
