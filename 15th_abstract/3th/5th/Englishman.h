#ifndef __ENGLISHMAN_H
#define __ENGLISHMAN_H

#include <iostream>
#include <string>
#include <unistd.h>
#include "Human.h"

using namespace std;

class Englishman : public Human {

public:

    void eating(void);
    void wearing(void);
    void driving(void);
    ~Englishman();
};

#endif /* __ENGLISHMAN_H */
