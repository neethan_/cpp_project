#ifndef __HUMAN_H
#define __HUMAN_H

#include <iostream>
#include <string>
#include <unistd.h>


using namespace std;


class Human {
private:
    char *name;

public:

    void setName(char *name);
    char *getName(void);

    // 添加virtual可以实现多态
    virtual void eating(void){cout<<"use hand to eat"<<endl;}
    virtual void wearing(void){}
    virtual void driving(void){}
};


#endif /* __HUMAN_H */
