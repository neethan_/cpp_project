#ifndef __HUMAN_H
#define __HUMAN_H

#include <iostream>
#include <string>
#include <unistd.h>


using namespace std;


class Human {
private:
    char *name;

public:

    void setName(char *name);
    char *getName(void);

};


#endif /* __HUMAN_H */
