#include <iostream>
#include <string.h>


using namespace std;

// 抽象出一个基类，将sofa和bed共有的部分提炼出来
class Furniture {
private:
    int weight;

public:
    Furniture(){
        cout<<"Furniture()"<<endl;
    }
    void setWeight(int w){
        this->weight = w;
    } 
    int getWeight(void){
        return this->weight;
    }     
};

class Vertification3C {

public:
    Vertification3C(){
        cout<< "vertification3C" <<endl;
    }
};

// 虚拟继承Funiturn
class Sofa : virtual public Furniture, virtual public Vertification3C {
private:
    int a;

public:
    Sofa(){
        cout<<"Sofa()"<<endl;
    }
    void watchTV(void){
        cout<<"sofa watch TV"<<endl;
    }     
};

class Bed : virtual public Furniture, virtual public Vertification3C{
private:
    int b;

public:
    Bed(){
        cout<<"Bed()"<<endl;
    }
    void sleep(void){
        cout<<"bed sleep"<<endl;
    }   

};

// 多重继承
class sofaBed:public Sofa,public Bed{
private:
    int c;

public:
    sofaBed(){
        cout<<"sofaBed()"<<endl;
    }
    sofaBed(char *abc){
        cout<<"sofaBed(char *abc)"<<endl;
    }
};

// 例如有一家左右公司生产沙发床
class LeftRightComp{

public:
    LeftRightComp(){
        cout<<"LeftRightComp()"<<endl;
    }
    LeftRightComp(char *abc){
        cout<<"LeftRightComp(char *abc)"<<endl;
    }
};

// 生产日期
class Date{

public:
    Date(){
        cout<<"Date()"<<endl;
    }
    Date(char *abc){
        cout<<"Date(char *abc)"<<endl;
    }
};

// 家具类型
class Type{

public:
    Type(){
        cout<<"Type()"<<endl;
    }
    Type(char *abc){
        cout<<"Type(char *abc)"<<endl;
    }
};

class LeftRightSofaBed : public sofaBed, public LeftRightComp {
private:
    Date date;
    Type type;

public:
    LeftRightSofaBed(){
        cout<< "LeftRightSofaBed()" <<endl;
    }
    // 直接通过形参传参
    LeftRightSofaBed(char *str1, char *str2, char *str3): sofaBed(str1), LeftRightComp(str2), date(str3){
        cout<< "LeftRightSofaBed(str)" <<endl;
    }

};

int main(int argc, char **argv)
{
// 构造函数执行顺序 ,其中虚拟基类构造函数只执行一次 
// <-----------------------------------------------
// sofaBed<Sofa<Vertification3C<Furniture
// LeftRightComp >LeftRightSofaBed

    LeftRightSofaBed s("abc", "123", "aa");
     
    return 0;
}