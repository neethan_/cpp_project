#include <iostream>


using namespace std;

int add_one(int a)
{
    a = a+1;
    return a;
}

// 指针 => 引用,&b 不是取址
int add_one_ref(int &b)
{
    b = b+1;
    return b;
}


int main(int argc, char **argv)
{
    int a = 99;

    cout<<add_one(a)<<endl;
    cout<<"a="<<a<<endl;

    // 直接将变量名传入即可
    cout<<add_one_ref(a)<<endl;
    cout<<"a="<<a<<endl;

    return 0;
}