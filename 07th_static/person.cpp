#include <iostream>
#include <string.h>

using namespace std;

class Person {
private:
    static int cnt; // 使用static修饰表示cnt属于整个Person类，只有一份，不属于某个实例化对象。这里只是申明，并没有分配内存空间
    char *name;
    int age;
    char *work;

public:
    // 构造函数，创建对象时自动执行
    Person(){
        name =NULL;
        work = NULL;
        cnt++;
    }
    Person(char *name){
        this->name = new char[strlen(name)+1];
        strcpy(this->name, name);
        this->work = NULL;
        cnt++;
    }
    Person(Person &per){
        cout<<"Person(Person &)"<<endl;
        this->age = per.age;

        this->name = new char[strlen(per.name)+1];
        strcpy(this->name, per.name);

        this->work = new char[strlen(per.work)];
        strcpy(this->work, per.work);
        cnt++;
    }

    // 析构函数，销毁对象时，自动执行
    ~Person(){
        cout<< "~Person()" <<endl;
        if(this->name){
            delete(this->name);
            cout<< "delete(this->name)" <<endl;
        } 
        if(this->work){
            delete(this->work);
            cout<< "delete(this->work)" <<endl;
        }       
    }

    // 此函数必须加上static
    static int get_Cnt(void){
        return cnt;
    }
    void setName(char *m){
        name = m;
    }

    int set_age(int a){
        if (a>=0 && a<110){
            age = a;
            return 0;
        } else {
            return -1;
        } 
    }

    void printInfo(void){
        cout<<"name= "<< name << "age= "<< age<< "work= "<< work<<endl;
    }
};

// 定义并初始化cnt，此时分配内存空间，不需要再使用static修饰
int Person::cnt = 0;

int main(int argc, char **argv)
{
    Person per1;
    Person per2;

    // 此静态函数不实例化对象也可以单独使用
    cout<< "Person cnt =" << Person::get_Cnt()<<endl;
    return 0;
}