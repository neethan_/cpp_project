#include <iostream>
#include <string.h>


using namespace std;


class Human {
private:
    int a;

public:
    // 将此基类的eating函数定义为虚函数
    virtual void eating(void){
        cout<<"use hand to eat"<<endl;
    }

    // 析构函数一般都申明为虚函数
    virtual ~Human(){
        cout<<"~Human()"<<endl;
    }
};

class Englishman : public Human {

public:
   virtual  ~Englishman(){
        cout<<"~Englishman()"<<endl;
    }

    void eating(void){
        cout<<"use knife to eat"<<endl;
    }
};

class Chinese : public Human{

public:
    virtual ~Chinese(){
        cout<<"~Chinese()"<<endl;
    }

    void eating(void){
        cout<<"use chopsticks to eat"<<endl;
    }
};

// &h => h 改成传值,则无多态
void test_eating(Human h)
{
    h.eating();
}

int main(int argc, char **argv)
{
    Human *h = new Human;
    Englishman *e = new Englishman;
    Chinese *c = new Chinese;

    Human *p[3] = {h, e, c};

    for(int i=0; i<3; i++){
        p[i]->eating();
        delete(p[i]);
    }
    return 0;
}