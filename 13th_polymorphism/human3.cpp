#include <iostream>
#include <string.h>


using namespace std;


class Human {
private:
    int a;

public:
    // 将此基类的eating函数定义为虚函数
    virtual void eating(void){
        cout<<"use hand to eat"<<endl;
    }
};

class Englishman : public Human {

public:
    void eating(void){
        cout<<"use knife to eat"<<endl;
    }
};

class Chinese : public Human{

public:
    void eating(void){
        cout<<"use chopsticks to eat"<<endl;
    }
};

// &h => h 改成传值,则无多态
void test_eating(Human h)
{
    h.eating();
}

int main(int argc, char **argv)
{
    Human h;
    Englishman e;
    Chinese c;

    // 需要分辨eating是哪个类的,因此 需要使用虚函数，在基类函数前使用virtual修饰即可
    test_eating(h);
    test_eating(e);
    test_eating(c);

    cout<<"sizeof(Human) = "<< sizeof(h) <<endl;
    cout<<"sizeof(Englishman) ="<< sizeof(e) <<endl;
    cout<<"sizeof(Chinese)= "<< sizeof(c) <<endl;
    return 0;
}