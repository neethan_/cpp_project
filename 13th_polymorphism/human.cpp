#include <iostream>
#include <string.h>


using namespace std;


class Human {

public:
    void eating(void){
        cout<<"use hand to eat"<<endl;
    }
};

class Englishman : public Human {

public:
    void eating(void){
        cout<<"use knife to eat"<<endl;
    }
};

class Chinese : public Human{

public:
    void eating(void){
        cout<<"use chopsticks to eat"<<endl;
    }
};

void test_eating(Human &h)
{
    h.eating();
}

int main(int argc, char **argv)
{
    Human h;
    Englishman e;
    Chinese c;

    // 此方法无法分辨是哪个类的eating,需要使用虚函数
    test_eating(h);
    test_eating(e);
    test_eating(c);

    return 0;
}