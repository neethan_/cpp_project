#include <iostream>
#include <string.h>


using namespace std;

class Sofa {
private:
    int weight;

public:
    void watchTV(void){
        cout<<"sofa watch TV"<<endl;
    }   

    void setWeight(int w){
        this->weight = w;
    } 
    int getWeight(void){
        return this->weight;
    }    
};

class Bed {
private:
    int weight;

public:
    void sleep(void){
        cout<<"bed sleep"<<endl;
    }   
    void setWeight(int w){
        this->weight = w;
    } 
    int getWeight(void){
        return this->weight;
    }  
};

// 多重继承
class sofaBed:public Sofa,public Bed{

};

int main(int argc, char **argv)
{
    sofaBed s;

    s.watchTV();
    s.sleep();

    s.setWeight(100); // error, 两个基类有相同的成员变量，此时访问无法分辨是哪个类
     
    return 0;
}