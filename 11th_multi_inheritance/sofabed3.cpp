#include <iostream>
#include <string.h>


using namespace std;

// 抽象出一个基类，将sofa和bed共有的部分提炼出来
class Furniture {
private:
    int weight;

public:
    void setWeight(int w){
        this->weight = w;
    } 
    int getWeight(void){
        return this->weight;
    }     
};

// 虚拟继承Funiturn
class Sofa : virtual public Furniture {
private:
    int a;

public:
    void watchTV(void){
        cout<<"sofa watch TV"<<endl;
    }     
};

class Bed : virtual public Furniture {
private:
    int b;

public:
    void sleep(void){
        cout<<"bed sleep"<<endl;
    }   

};

// 多重继承
class sofaBed:public Sofa,public Bed{
private:
    int c;
};

int main(int argc, char **argv)
{
    sofaBed s;

    s.watchTV();
    s.sleep();

    s.setWeight(100); // 正确, 两个基类有相同的成员变量，此时访问无法分辨是哪个类，需要抽象出一个基类，采用虚拟继承
     
    return 0;
}