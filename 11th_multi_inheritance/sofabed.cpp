#include <iostream>
#include <string.h>


using namespace std;

class Sofa {
public:
    void watchTV(void){
        cout<<"sofa watch TV"<<endl;
    }    
};

class Bed {
public:
    void sleep(void){
        cout<<"bed sleep"<<endl;
    }    
};

// 多重继承
class sofaBed:public Sofa,public Bed{

};

int main(int argc, char **argv)
{
    sofaBed s;

    s.watchTV();
    s.sleep();

    return 0;
}